# Create your views here.
from rest_framework import viewsets
from django_filters import rest_framework as filters
from django.views import generic
from rest_framework.response import Response

import urllib.parse
import ast

from .serializers import SliceSerializer, ShotSerializer, BasicShotSerializer
from .models import EntryData, Entry
from .filters import entry_data_Filter
from .search import search_shots, search_slices

#API VIEWS

class VarViewSet(viewsets.ModelViewSet):

    serializer_class = SliceSerializer
    queryset = EntryData.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = entry_data_Filter


class WESTViewSet(viewsets.ModelViewSet):
    serializer_class = ShotSerializer
    queryset = Entry.objects.all()


class ShotViewSet(viewsets.ViewSet):
    def list(self, request):

        try:
            full_output_bool = request.query_params.get('full')
        except ValueError:
            full_output_bool=True
        
        try:
            variables = request.query_params.get('inputs')
            variables = ast.literal_eval(variables)
            queryset = search_shots(variables)
        
        except ValueError:
            queryset = Entry.objects.all()
        

        if full_output_bool=='True':
            serializer = ShotSerializer(queryset, many=True)
        else:
            serializer = BasicShotSerializer(queryset, many=True)
        return Response(serializer.data)


class SliceViewSet(viewsets.ViewSet):
    def list(self, request):

        variables = request.query_params.get('inputs')
        variables = ast.literal_eval(variables)

        queryset = search_slices(variables)


        #unsure about this. Initially not needed and not needed in ShotViewSet
        serializer_context = {
            'request': request,
        }
        
        serializer = SliceSerializer(queryset, context=serializer_context, many=True)

        return Response(serializer.data)