from rest_framework import serializers
from django_filters.rest_framework import DjangoFilterBackend

from .models import EntryData, Entry, Machines, Variables

class SliceSerializer(serializers.HyperlinkedModelSerializer):
    
    variable_name = serializers.ReadOnlyField(source='variable.variable_name')
   
    value = serializers.CharField(source='variable_value', read_only=True)
    time = serializers.CharField(read_only=True)
    
    class Meta:
        model = EntryData
        fields = ('variable_name', 'entry_id', 'value', 'time')


class ShotSerializer(serializers.HyperlinkedModelSerializer):

    data = SliceSerializer(source='data_entries', many=True)
    entry_count = serializers.ReadOnlyField(source='data_entries.count')
    machine_name = serializers.ReadOnlyField(source='machine.machine_name')
    class Meta:
        model = Entry
        fields = ('id','machine_name', 'shot', 'run', 'ds_version', 'entry_count', 'data')


class BasicShotSerializer(serializers.HyperlinkedModelSerializer):
    entry_count = serializers.ReadOnlyField(source='data_entries.count')
    machine_name = serializers.ReadOnlyField(source='machine.machine_name')

    class Meta:
        model = Entry
        fields = ('id','machine_name', 'shot', 'run', 'ds_version', 'entry_count')


    
