
from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()


router.register(r'var', views.VarViewSet)
router.register(r'west', views.WESTViewSet)
router.register(r'slices', views.SliceViewSet, basename='slice')
router.register(r'shots', views.ShotViewSet, basename='shots')


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

]
