from .models import EntryData, Entry

#the search functionality used for the ShotViewSets

def filter_by_variable(variable_dict):
    
    variable_name = variable_dict['var_name']
    variable_set = EntryData.objects.filter(variable__variable_name=variable_name)

    try:
        variable_max = variable_dict['var_max']
        variable_set = variable_set.filter(variable_value__lte=variable_max)
    except KeyError:
        pass
    

    try:
        variable_min = variable_dict['var_min']
        variable_set = variable_set.filter(variable_value__gte=variable_min)
    except KeyError:
        pass
    
    return variable_set


def search_shots(variables):

    queryset = Entry.objects.all()

    for variable_dict in variables:

        #returns queryset of entrydata
        variable_set = filter_by_variable(variable_dict)
        #gets all the entry ids of the data
        variable_ids = variable_set.values_list('entry').distinct()
        #creates a new queryset with the entry Ids and then finds the intersection with the old queryset
        queryset = queryset & queryset.filter(id__in=variable_ids).all()

    return queryset.distinct()
    

def search_slices(variables):
    queryset = EntryData.objects.all()

    for variable_dict in variables:
        queryset = queryset & filter_by_variable(variable_dict)

    return queryset

