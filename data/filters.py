
from django_filters import rest_framework as filters

from .models import EntryData, Entry


class entry_data_Filter(filters.FilterSet):
    id = filters.NumberFilter()
    class Meta:
        model = EntryData
        fields = ["variable_id"]

