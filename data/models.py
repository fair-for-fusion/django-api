# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Annotations(models.Model):
    id = models.BigAutoField(primary_key=True)
    entry_id = models.BigIntegerField(blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    user_id = models.BigIntegerField(blank=True, null=True)
    date_added = models.DateTimeField()
    interval_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'annotations'


class CatalogParameters(models.Model):
    immediate_execution = models.IntegerField(blank=True, null=True)
    update_frequency = models.BigIntegerField(blank=True, null=True)
    xml_storage = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'catalog_parameters'


class Machines(models.Model):
    id = models.BigAutoField(primary_key=True)
    machine_name = models.CharField(max_length=16, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'machines'


class Variables(models.Model):
    id = models.BigAutoField(primary_key=True)
    variable_name = models.CharField(max_length=128, blank=True, null=True)
    variable_definition = models.TextField(blank=True, null=True)
    cpo_name = models.CharField(max_length=128, blank=True, null=True)
    cpo_field = models.CharField(max_length=256, blank=True, null=True)
    variable_type = models.SmallIntegerField(blank=True, null=True)
    time_dependent = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'variables'


class Entry(models.Model):
    id = models.BigAutoField(primary_key=True)
    #machine_id = models.BigIntegerField(blank=True, null=True)
    machine = models.ForeignKey(Machines, on_delete=models.CASCADE)
    shot = models.BigIntegerField(blank=True, null=True)
    user_id = models.BigIntegerField(blank=True, null=True)
    run = models.BigIntegerField(blank=True, null=True)
    ds_version = models.CharField(max_length=32, blank=True, null=True)
    occurrence = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'entry'


class EntryData(models.Model):
    id = models.BigAutoField(primary_key=True)
    entry = models.ForeignKey(Entry, on_delete=models.CASCADE, related_name="data_entries")
    #variable_id = models.BigIntegerField(blank=True, null=True)
    variable = models.ForeignKey(Variables, on_delete=models.CASCADE)
    variable_value = models.CharField(max_length=256, blank=True, null=True)
    interval_id = models.BigIntegerField(blank=True, null=True)
    variable_value_double = models.FloatField(blank=True, null=True)
    variable_value_int = models.BigIntegerField(blank=True, null=True)
    time = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'entry_data'

    def __str__(self):
        return '{%s: %s, %s: %s}' % ('value', self.variable_value, 'time', self.time)


class Intervals(models.Model):
    id = models.BigAutoField(primary_key=True)
    time_min = models.FloatField(blank=True, null=True)
    time_max = models.FloatField(blank=True, null=True)
    entry_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'intervals'


class Outcome(models.Model):
    id = models.BigAutoField(primary_key=True)
    scheduler_id = models.BigIntegerField(blank=True, null=True)
    outcome = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'outcome'


class Scheduler(models.Model):
    id = models.BigAutoField(primary_key=True)
    submission_date = models.DateTimeField(blank=True, null=True)
    user_name = models.CharField(max_length=32, blank=True, null=True)
    machine_name = models.CharField(max_length=16, blank=True, null=True)
    shot = models.BigIntegerField(blank=True, null=True)
    run = models.BigIntegerField(blank=True, null=True)
    ds_version = models.CharField(max_length=32, blank=True, null=True)
    occurrence = models.IntegerField(blank=True, null=True)
    simulation_model = models.CharField(max_length=256, blank=True, null=True)
    mode = models.IntegerField(blank=True, null=True)
    xml_file = models.CharField(max_length=256, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'scheduler'


class Users(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_name = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'



